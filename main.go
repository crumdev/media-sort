package main

import (
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/rwcarlsen/goexif/exif"
	"github.com/rwcarlsen/goexif/tiff"
)

func main() {

	source := flag.String("src", "./", "Source folder containing the images to sort (default './')")
	destination := flag.String("dest", "./", "Destination folder to place the sorted images in YYYY>MM folder structure (default: './')")
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Made and maintianed by gitlab.com/crumdev\n\n")
		flag.PrintDefaults()
	}
	flag.Parse()

	// Replace the extensions below with the extensions you want to consider.
	extensions := []string{".jpg", ".jpeg", ".png", ".tiff", ".gif", ".mp4", ".mov", ".nef", "dng", "cr2", ""}

	fmt.Printf("Source folder set to [%v]\n", source)
	fmt.Printf("Destination folder set to [%v]\n", destination)

	var processFiles = func(xpath string, xinfo fs.DirEntry, xerr error) error {
		if xerr != nil {
			fmt.Printf("error [%v] at path [%q]\n", xerr, xpath)
			return xerr
		}
		// Only consider files with the extensions in the provided array.
		ext := strings.ToLower(filepath.Ext(xpath))
		var found bool
		for _, e := range extensions {
			if strings.EqualFold(ext, e) {
				found = true
				break
			}
		}
		if !found {
			return nil
		}

		// dateTakenParsed, err := time.Parse("2006:01:02 15:04:05", ParseCreationDate(xpath))
		// if err != nil {
		// 	fmt.Printf("Error occured when parsing the created time for [%v]: %v\n", xpath, err)
		// 	return err
		// }

		// // Create the destination directory.
		// destDir := filepath.Join(*destination, fmt.Sprintf("%04d/%02d", dateTakenParsed.Year(), dateTakenParsed.Month()))

		// createDirectoryIfNotExist(destDir)

		// moveTheFile(xpath, destDir)

		fmt.Printf("%v\n", xpath)
		return nil
	}

	err := filepath.WalkDir(*source, processFiles)
	if err != nil {
		return
	}
}

func moveTheFile(xpath string, destDir string) error {
	// Move the file to the destination directory.
	destPath := filepath.Join(destDir, filepath.Base(xpath))
	err := os.Rename(xpath, destPath)
	if err != nil {
		fmt.Printf("Error moving file [%v] to [%v]", xpath, destDir)
		return err
	}
	return nil
}

func getTimeAsBytes() []byte {
	// Get the current time.
	now := time.Now()

	// Format the time as a string in "YYYY:MM:DD HH:MM:SS" format.
	timeStr := now.Format("2006:01:02 15:04:05")

	// Convert the string to a byte slice.
	b := []byte(timeStr)

	return b
}

func createDirectoryIfNotExist(dirPath string) error {
	// Check if the directory already exists.
	_, err := os.Stat(dirPath)
	if err == nil {
		// The directory already exists.
		return nil
	}

	// Create the directory.
	err = os.MkdirAll(dirPath, 0755)
	if err != nil {
		// Failed to create the directory.
		fmt.Printf("Failed to create directory [%v]\n", dirPath)
		return err
	}

	// Directory created successfully.
	return nil
}

func ParseCreationDate(xpath string) (dateStr string) {
	// Open the file.
	f, err := os.Open(xpath)
	if err != nil {
		fmt.Printf("Error occured opening [%v]: %v\n", xpath, err)
		return ""
	}
	defer f.Close()

	// Parse the EXIF data.
	x, err := exif.Decode(f)
	if err != nil {
		fmt.Printf("Error occured decoding [%v]: %v\n", xpath, err)
		return ""
	}

	// Get the date taken attribute.
	dateTaken, err := x.Get(exif.DateTimeOriginal)
	if err != nil {
		// If the date taken attribute is missing, use the file's modification time instead.
		dateTaken = &tiff.Tag{Id: 306, Count: 1, Type: tiff.DTAscii, Val: getTimeAsBytes()}
	}

	// Parse the date taken attribute.
	dateTakenStr, err := dateTaken.StringVal()
	if err != nil {
		fmt.Printf("Error returning StringVal of dateTaken: %v\n", err)
		return ""
	}

	return dateTakenStr
}
